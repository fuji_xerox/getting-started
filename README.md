BitBucketの使い方
===============



### 0. アカウント作成とアクティベート
<https://bitbucket.org/account/signup/> からサインアップして下さい．アカウント名に社員番号等は使わないで下さい．一見して分かりにくいので．届いたメールのリンクをクリックしてアクティベートして下さい．既に個人アカウントを持っていたら．会社ドメインのメールアドレスを追加し，アクティベートして下さい．(GitHubのアカウントがある場合は、OAuthを使ってアカウントを作ることをオススメします．)

### 1. アカウント名を連絡
下記のフォーマットで、アカウント名を<github@fujixerox.co.jp>に連絡して下さい．組織(チーム)への追加が終わったら，利用する専用ProxyのURIをお伝えします．

### 2. プロファイルの設定
<https://bitbucket.org/account/user/{{userid}}/> でName，Locationを設定して下さい．{{userid}}の箇所は、自分のアカウント名に置き換えて下さい．  

```
Name:     Taro Fuji
Location: Kanagawa, Japan.
```

### 3. Gitのインストールと設定
まだ利用する端末にGitがインストールしていない場合は、<http://git-scm.com/>からパッケージをダウンロードし、インストールして下さい．

#### Windows
上記のリンクから、exeファイルをダウンロードし実行して下さい．Git BashというTerminalソフトウェアがインストールされています．そのソフトウェアでGitコマンドが使えます．

#### Mac OS X
上記のリンクから、dmgファイルをダウンロードし実行して下さい．TerminalからGitコマンドが使えます．

インストール後にユーザ名とメールアドレスを設定して下さい．
このメールアドレスはBitBucketアカウントの個人設定(<https://bitbucket.org/account/user/{{userid}}/email/>)に登録しているものを使って下さい．
Windowsの場合は、git bash, Mac OS Xの場合は、terminalを利用して下さい.

```
> git config --global user.name "Taro Fuji"
> git config --global user.email taro.fuji@example.com
```

### 4. SSHの公開鍵登録
<https://bitbucket.org/account/user/{{userid}}/ssh-keys/> から公開鍵を登録して下さい．鍵ペアが無ければ`ssh-keygen`コマンドで生成して下さい．秘密鍵と公開鍵を絶対に間違えないで下さい．

* <https://confluence.atlassian.com/display/BITBUCKET/How+to+install+a+public+key+on+your+Bitbucket+account>

### 5. 接続のための設定
社内ネットワークからリポジトリへアクセスするためには，下記のいずれかの設定が必要です．

### 5.1. GitのProxyを設定
`example.com:8000` の部分はお伝えしたURIに置き換えて下さい．
```
> git config --global http.proxy example.com:8000
```

### 5.2. connectコマンドを使ったSSH接続
BitBucketを利用する端末に，`connect`コマンドをインストールして下さい．
 
 * <http://takuya-1st.hatenablog.jp/entry/20110813/1313223707>
 * <http://www.meadowy.org/~gotoh/ssh/connect.c>

WindowsであればGit bashに標準でインストールされています.

Mac OS Xであれば`homebrew`からインストールできます．
```
> brew install connect
```


次に`~/.ssh/config`に下記を加えて下さい．`expample.com:8000` の部分はお伝えしたURIに置き換えて下さい．
```
Host bitbucket.org
  Port 22
  ProxyCommand connect -H example.com:8000 %h %p
```

### 6. 接続の確認
下記のコマンドを入力し，この文章のリポジトリをcloneできることを確認して下さい．
```
> git clone git@bitbucket.org:fuji_xerox_co_ltd/getting-started.git
```
or
```
> git clone https://{{userid}}@bitbucket.org/fuji_xerox_co_ltd/getting-started.git
```

手順の中で分からないことがあれば，<github@fujixerox.co.jp>へ連絡して下さい．
